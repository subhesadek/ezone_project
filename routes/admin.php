<?php

Route::get('login','Admin\LoginController@adminlogin')->name('admin.login.get');
Route::post('login','Admin\LoginController@login')->name('admin.login');
Route::get('admin-forgot-password','Admin\ForgotPasswordController@adminLinkRequestForm')->name('admin.forgot.password');
Route::post('admin-password-mail','Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
Route::get('admin-password-reset/{token}','Admin\ResetPasswordController@resetPassword')->name('admin.password.reset');
Route::post('admin-password-update','Admin\ResetPasswordController@reset')->name('admin.password.update');



Route::group(['middleware'=>'auth:admin'],function (){
    Route::get('dashboard','AdminController@dashboard')->name('admin.dashboard');
    Route::get('admin-logout','AdminController@logout')->name('admin.logout');
    Route::get('admin-register','AdminController@register')->name('admin.register');
    Route::post('admin-register','AdminController@adminRegister')->name('admin.register.post');
                        //** Category **/
    Route::get('/category','Admin\category\CategoryController@index')->name('admin.category');
    Route::get('/category/getTableData','Admin\category\CategoryController@get')->name('admin.category.getTableData');
    Route::get('/category/edit/{id}','Admin\category\CategoryController@edit')->name('admin.category.getEditData');
    Route::post('/category','Admin\category\CategoryController@create');
    Route::post('/category/update','Admin\category\CategoryController@update')->name('admin.category.update');
    Route::get('/category/delete/{id}','Admin\category\CategoryController@delete')->name('admin.category.delete');
    Route::get('/category/publish/{id}','Admin\category\CategoryController@publish')->name('category.publish');
    Route::get('/category/unpublish/{id}','Admin\category\CategoryController@unpublish')->name('category.unpublish');
                        //** Brand **/
    Route::get('/brand','Admin\category\BrandController@index')->name('admin.brand');
    Route::get('/brand/getTableData','Admin\category\BrandController@get')->name('admin.brand.getTableData');
    Route::get('/brand/edit/{id}','Admin\category\BrandController@edit')->name('admin.brand.getEditData');
    Route::post('/brand/inlineedit','Admin\category\BrandController@inlineEdit')->name('admin.brand.inlineEdit');
    Route::post('/brand','Admin\category\BrandController@create');
    Route::post('/brand/update','Admin\category\BrandController@update')->name('admin.brand.update');
    Route::get('/brand/delete/{id}','Admin\category\BrandController@delete')->name('admin.brand.delete');
    Route::get('/brand/publish/{id}','Admin\category\BrandController@publish')->name('brand.publish');
    Route::get('/brand/unpublish/{id}','Admin\category\BrandController@unpublish')->name('brand.unpublish');
                            //** Subcategory **/
    Route::get('/subcategory','Admin\category\SubcategoryController@index')->name('admin.subcategory');
    Route::get('/subcategory/getTableData','Admin\category\SubcategoryController@get')->name('admin.subcategory.getTableData');
    Route::get('/subcategory/edit/{id}','Admin\category\SubcategoryController@edit')->name('admin.subcategory.getEditData');
    Route::post('/subcategory','Admin\category\SubcategoryController@create');
    Route::post('/subcategory/update','Admin\category\SubcategoryController@update')->name('admin.subcategory.update');
    Route::get('/subcategory/delete/{id}','Admin\category\SubcategoryController@delete')->name('admin.subcategory.delete');
    Route::get('/subcategory/publish/{id}','Admin\category\SubcategoryController@publish')->name('subcategory.publish');
    Route::get('/subcategory/unpublish/{id}','Admin\category\SubcategoryController@unpublish')->name('subcategory.unpublish');
                            //** Coupon **/
    Route::get('/coupon','Admin\CouponController@index')->name('admin.coupon');
    Route::get('/coupon/getTableData','Admin\CouponController@get')->name('admin.coupon.getTableData');
    Route::get('/coupon/edit/{id}','Admin\CouponController@edit')->name('admin.coupon.getEditData');
    Route::post('/coupon','Admin\CouponController@create');
    Route::post('/coupon/update','Admin\CouponController@update')->name('admin.coupon.update');
    Route::get('/coupon/delete/{id}','Admin\CouponController@delete')->name('admin.coupon.delete');
    Route::get('/coupon/publish/{id}','Admin\CouponController@publish')->name('coupon.publish');
    Route::get('/coupon/unpublish/{id}','Admin\CouponController@unpublish')->name('coupon.unpublish');
                            //** Product **/
    Route::get('/product/all', 'Admin\ProductController@index')->name('all.product');
    Route::get('/product/add', 'Admin\ProductController@create')->name('add.product');
    Route::post('/store/product', 'Admin\ProductController@store')->name('store.product');
    Route::get('inactive/product/{id}', 'Admin\ProductController@Inactive');
    Route::get('active/product/{id}', 'Admin\ProductController@Active');
    Route::get('delete/product/{id}', 'Admin\ProductController@DeleteProduct');
    Route::get('view/product/{id}', 'Admin\ProductController@ViewProduct');
    Route::get('edit/product/{id}', 'Admin\ProductController@EditProduct');
    Route::post('update/product/withoutphoto/{id}', 'Admin\ProductController@UpdateProductWithoutPhoto');
    Route::post('update/product/photo/{id}', 'Admin\ProductController@UpdateProductPhoto'); 
    Route::get('get/subcategory/{category_id}', 'Admin\ProductController@GetSubcat');
    //Blog Routes---
    Route::get('/add/post', 'Admin\PostController@create')->name('add.blogpost');
    Route::post('/store/post', 'Admin\PostController@store')->name('store.post');
    Route::get('/all/post', 'Admin\PostController@index')->name('all.blogpost');
    Route::get('delete/post/{id}', 'Admin\PostController@destroy');
    Route::get('edit/post/{id}', 'Admin\PostController@edit');
    Route::post('update/post/{id}', 'Admin\PostController@update');







}); 
