<?php

use Illuminate\Support\Facades\Route;


 
Route::get('/', function () {
    return view('layouts.app');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');


//** Public Routes **/
Route::get('/all-category','frontend\MainController@getCategories');
Route::get('/all-subcategory','frontend\MainController@getSubcategories');
Route::get('/all-brands','frontend\MainController@getBrands');
Route::get('/all-products','frontend\MainController@getProducts');
Route::get('/cat-products/{id}','frontend\MainController@getcatProducts');




// Route::get('/category/{id}','MainController@getCatProducts');
// Route::get('/featured-product','frontend\MainControlle@getFeaturedProducts');
// Route::get('/{any}', 'AdminController@dashboard')->where('any', '.*');





//** Admin Routes **/
Route::prefix('admin')->group(base_path('routes/admin.php'));