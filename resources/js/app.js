
require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';		//import vue router
Vue.use(VueRouter);

import Vuex from 'vuex';					//import vuex
Vue.use(Vuex);

import storeData from './store/store';
const store = new Vuex.Store(
    storeData
)

//  files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('header-section', require('./components/public/inc/header.vue').default);
Vue.component('footer-section', require('./components/public/inc/footer.vue').default);
Vue.component('content-section', require('./components/public/master.vue').default);


//all vue routes from routes.js file
import { routes } from './routes';

const router = new VueRouter({
    routes,
    mode: 'history',
})

const app = new Vue({
    el: '#app',
    router,
    store
});
