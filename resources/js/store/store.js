export default {
    state: {
        category: [],
        product: [],
        catProduct: []
    },
    getters: {
        getCategory(state) {
            return state.category
        },
        getProduct(state) {
            return state.product
        },
        getCatProduct(state) {
            return state.catProduct
        }
    },
    actions: {
        allCategory(context) {
            axios.get('/all-category')
                .then((response) => {
                    context.commit('categories', response.data.categories)
                })
        },
        allProduct(context) {
            axios.get('/all-products')
                .then((response) => {
                    context.commit('products', response.data.products)
                })
        },
        CategoryByID(context, payload) {
            axios.get('/cat-products/' + payload)
                .then((response) => {
                    context.commit('getProductbyId', response.data.products)
                })
        }
    },
    mutations: {
        categories(state, data) {
            return state.category = data
        },
        products(state, data) {
            return state.product = data
        },
        getProductbyId(state, data) {
            return state.catProduct = data
        }
    }
}