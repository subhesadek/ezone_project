                                @php($sl = 1)
                                        @foreach($brands as $brand)
                                        <tr>
                                            <td>{{ $sl++ }}</td>
                                            <td id="t_brand_name" data-id1="{{ $brand->id }}" ondblclick="this.contentEditable=true" onblur="this.contentEditable=false" class="text-capitalize">{{ $brand->brand_name }}</td>
                                            
                                            <td>
                                                @if($brand->brand_logo == '')
                                                No Image
                                                @else
                                                <img src="{{ asset($brand->brand_logo) }}" height="50px" width="50px" alt="brand image">
                                                @endif 
                                            </td>
                                            <td>
                                                @if($brand->brand_status == 1)
                                                <a id="{{$brand->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                                                    <i class="fa fa-arrow-up"></i>
                                                </a>
                                                @else
                                                <a id="{{$brand->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                                                    <i class="fa fa-arrow-down"></i>
                                                </a> 
                                                @endif
                                            </td>
                                            <td>
                                                <a id="{{$brand->id}}" href="#editBrandModal" class="btn btn-success edit" data-toggle="modal">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a id="{{$brand->id}}" href="" class="btn btn-danger delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        {{--Edit Brand modal here--}}
                             @include('admin.brand.edit-brand')
                                        @endforeach