@extends('admin.admin_layouts')

@section('admin_content')

      <div class="sl-page-title">
        <h5>Brand Table</h5>
        </div><!-- sl-page-title -->

      <div class="card pd-20 pd-sm-40">
        <span class="h4">Brand List</span>
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addBrandModal">
        <i class="fa fa-plus"><b> Add New</b></i>
        </button>
        <br>
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Brand Name</th>
                <th>Image</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="tableData">
              @php($sl = 1)
              @foreach($brands as $brand)
              <tr>
                  <td>{{ $sl++ }}</td>
                  <td id="t_brand_name" class="text-capitalize">{{ $brand->brand_name }}</td>
                  <td>
                      @if($brand->brand_logo == '')
                      No Image
                      @else
                      <img src="{{ asset($brand->brand_logo) }}" height="50px" width="50px" alt="brand image">
                      @endif
                  </td>
                  <td>
                      @if($brand->brand_status == 1)
                      <a id="{{$brand->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                          <i class="fa fa-arrow-up"></i>
                      </a>
                      @else
                      <a id="{{$brand->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                          <i class="fa fa-arrow-down"></i>
                      </a>
                      @endif
                  </td>
                  <td>
                      <a id="{{$brand->id}}" href="#editBrandModal" class="btn btn-success edit" data-toggle="modal">
                          <i class="fa fa-edit"></i>
                      </a>
                      <a id="{{$brand->id}}" href="" class="btn btn-danger delete">
                          <i class="fa fa-trash"></i>
                      </a>
                  </td>
              </tr>
              
              @endforeach
          </tbody>

          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script>
    
        $(document).ready( function () {
            //for datatable
            // $('#datatable1').DataTable();
    
            //load table via ajax
            function loadDataTable(){
                $.ajax({
                    url: "{{ route('admin.brand.getTableData') }}",
                    success: function(data){
                        $('#tableData').html(data);
                    }
                })
            };
            loadDataTable();
            //add new brand data
            $('#addBrandForm').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.brand') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        
                        if (data == "done") {
                            $('#addBrandModal').modal('hide');
                            $('#addBrandForm')[0].reset();
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Brand Inserted Successfully !!'
                            })
                        }
                    },
                     error: function (error) {
                        $('#createBrandMessage').html('');

                        $('#createBrandMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#createBrandMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // create modal set to default
            $('#addBrandModal').on('hidden.bs.modal', function (e) {
                let input = $('#addBrandForm #brand_name');
                $('#addBrandForm').find('#createBrandMessage').html('');
                $(input).val('');
            })

        //brand publish
        $(document).on('click', '.publish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/brand/publish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //brand unpublish
        $(document).on('click', '.unpublish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/brand/unpublish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                    
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //show data for edit modal
        $(document).on('click', '.edit', function(e){
            $('#editBrandModal').modal('show');
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/brand/edit')}}/"+id,
                method: "GET",
                success: function(data){
                        $('#edit_id').val(data.id);
                        $('#edit_brand_name').val(data.brand_name);
                        $('#previewHolder2').attr('src', "{{asset('')}}"+data.brand_logo);
                }
            })
        });
        //update brand
        $('#editBrandForm').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.brand.update') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if (data == "done") {
                            $('#editBrandModal').modal('hide');
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Brand Updated Successfully !!'
                            })
                        }
                    },
                    error: function (error) {
                        $('#updateBrandMessage').html('');

                        $('#updateBrandMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#updateBrandMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // Update modal set to default
            $('#editBrandModal').on('hidden.bs.modal', function (e) {
            let input = $('#editBrandForm #edit_brand_name');
            $('#editBrandForm').find('#updateBrandMessage').html('');
            $(input).val('');
            })
        //delete brand
        $(document).on('click', '.delete', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                      if (result.value) {
                        $.ajax({
                            url: "{{url('admin/brand/delete')}}/"+id,
                            method: "GET",
                            success: function(data){
                                if (data == "done") {
                                    loadDataTable();
                                }
                            }
                        })
                        Swal.fire(
                          'Deleted!',
                          'Brand has been deleted.',
                          'success'
                        )
                      }
                    })
            
        });
        //inline edit brand name
        $(document).on('blur', '#t_brand_name', function(){
            var id = $(this).data("id1");
            var text = $(this).text();
            inlineEdit(id, text, "brand_name");
        });

        //inline edit ajax request
        function inlineEdit(id, text, column_name){
            $.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                method: "POST",
                url: "{{ route('admin.brand.inlineEdit') }}",
                data:{
                    id: id,
                    text: text,
                    column_name: column_name
                },
                success: function(data){
                    if (data == "done") {
                        alert("Updated");
                    }
                }
            });
        };
    
        //show modal after error occured
        @if (count($errors) > 0)
            $('#addBrandModal').modal('show');
        @endif
        //uploaded image preview
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#previewHolder').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
            }
            $("#filePhoto").change(function() {
                readURL(this);
            });
        } );
        //popover
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
        //tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
     
        {{--Add Brand modal here--}}
        @include('admin.brand.add-brand')

        {{--Edit Brand modal here--}}
        @include('admin.brand.edit-brand')

@endsection