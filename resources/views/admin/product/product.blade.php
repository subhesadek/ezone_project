@extends('admin.admin_layouts')

@section('admin_content')

<div class="sl-page-title">
  <h5>Product Table</h5>
  </div><!-- sl-page-title -->

<div class="card pd-20 pd-sm-40">

  <span class="h4">Product List</span>
  <a class="btn btn-primary float-right" href="{{ route('add.product') }}">
  <i class="fa fa-plus"><b> Add New Product</b></i>
  </a>

  <br>
  <div class="table-wrapper">
    <table id="datatable1" class="table display responsive nowrap table-responsive-sm">
      <thead>
        <tr>
          <th class="wd-5p"> ID</th>
          <th class="wd-15p">Product Code</th>
          <th class="wd-15p">Product Name</th>
          <th class="wd-15p">Image</th>
          <th class="wd-15p">Category</th>
          <th class="wd-15p">Brand</th>
          <th class="wd-15p">Qty</th>
          <th class="wd-15p">Status</th>
          <th class="wd-25p">Action</th>
        </tr>
      </thead>
      <tbody>
        @php($sl = 1)
        @foreach($product as $row)
        <tr>
          <td>{{ $sl++ }}</td>
          <td>{{$row->product_code}}</td>
          <td>{{$row->product_name}}</td>
          <td><img src="{{ URL::to($row->image_one)}}" width="50px;" height="50px;"></td>
          <td>{{$row->cat_name}}</td>
          <td>{{$row->brand_name}}</td>
          <td>{{$row->product_quantity}}</td>
          <td>
            @if($row->status == 1)
            <a href="{{ URL::to('admin/inactive/product/' .$row->id)}}" class="btn btn-sm btn-success" title="Active">
                <i class="fa fa-arrow-up"></i>
            </a>
            @else
            <a href="{{ URL::to('admin/active/product/' .$row->id)}}" class="btn btn-sm btn-danger" title="Inactive">
                <i class="fa fa-arrow-down"></i>
            </a>
            @endif
          </td>
          <td>
          <a href="{{ URL::to('admin/edit/product/' .$row->id)}}" class="btn btn-sm btn-info" title="Edit"><i class="fa fa-edit"></i></a>
          <a href="{{ URL::to('admin/delete/product/' .$row->id)}}" class="btn btn-sm btn-danger" id="delete" title="Delete"><i class="fa fa-trash"></i></a>
          <a href="{{ URL::to('admin/view/product/' .$row->id)}}" class="btn btn-sm btn-warning" title="View"><i class="fa fa-eye"></i></a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div><!-- table-wrapper -->
</div><!-- card -->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>  
  $(document).on("click", "#delete", function(e){
      e.preventDefault();
      var link = $(this).attr("href");
      Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
          })
         .then((result) => {
           if (result.value) {
                window.location.href = link;
           } else {
             swal("Safe Data!");
           }
         });
     });
</script>
@endsection