<div class="modal fade" id="addCouponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mg-r-100 mg-l-100 " id="exampleModalLabel">Add Coupon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>  
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form id="addCouponForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span id="createCouponMessage"></span>
                    <div class="form-group">
                        <label for="cat_name">Coupon Name</label>
                        <input type="text" name="coupon_name" class="form-control" id="coupon_name" placeholder="Enter Coupon Name">
                    </div>
                    <div class="form-group">
                        <label for="cat_name">Discount</label>
                        <input type="number" min="1" max="100" name="discount_name" class="form-control" id="discount_name">
                    </div>
                    <div class="form-group"> 
                        <label for="coupon_status">Publication Status</label>
                        <select class="form-control" name="coupon_status" id="coupon_status">
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="add_button" class="btn btn-primary">Add Coupon</button>
                </div>
            </form>
        </div>
    </div>
</div>
