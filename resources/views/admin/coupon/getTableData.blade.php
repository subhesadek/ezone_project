                                @php($sl = 1)
                                        @foreach($coupons as $coupon)
                                        <tr>
                                            <td>{{ $sl++ }}</td>
                                            <td id="t_coupon_name" data-id1="{{ $coupon->id }}">{{ $coupon->coupon_name }}</td> 
                                            <td id="t_discount_name" data-id1="{{ $coupon->id }}">{{ $coupon->discount_name }}</td>
                                            <td>
                                                @if($coupon->coupon_status == 1)
                                                <a id="{{$coupon->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                                                    <i class="fa fa-arrow-up"></i>
                                                </a>
                                                @else
                                                <a id="{{$coupon->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                                                    <i class="fa fa-arrow-down"></i>
                                                </a> 
                                                @endif
                                            </td>
                                            <td>
                                                <a id="{{$coupon->id}}" href="#editCouponModal" class="btn btn-success edit" data-toggle="modal">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a id="{{$coupon->id}}" href="" class="btn btn-danger delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        {{--Edit coupon modal here--}} 
        @include('admin.coupon.edit-coupon')
                                        @endforeach