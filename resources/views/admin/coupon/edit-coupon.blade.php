<div class="modal fade" id="editCouponModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mg-r-100 mg-l-100" id="exampleModalLabel">Edit Coupon</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger"> 
                <ul> 
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div> 
            @endif
            <form id="editCouponForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="edit_id">
                <div class="modal-body">
                    <span id="updateCouponMessage"></span>
                    <div class="form-group">
                        <label for="cat_name">Coupon Name</label>
                        <input type="text" name="coupon_name" class="form-control" id="edit_coupon_name" placeholder="Enter Coupon Name">
                    </div>
                    <div class="form-group">
                        <label for="cat_name">Discount</label>
                        <input type="number" min="1" max="100" name="discount_name" class="form-control" id="edit_discount_name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Coupon</button>
                </div>
            </form>
        </div> 
    </div>
</div>
