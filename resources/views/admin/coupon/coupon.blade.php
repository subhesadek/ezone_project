@extends('admin.admin_layouts')

@section('admin_content')

      <div class="sl-page-title">
        <h5>Coupon Table</h5>
        </div><!-- sl-page-title -->

      <div class="card pd-20 pd-sm-40">
        <span class="h4">Coupon List</span>
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addCouponModal">
        <i class="fa fa-plus"><b> Add New</b></i>
        </button>
        <br> 
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap table-sm">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Coupon Name</th>
                <th>Discount(%)</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="tableData">
              @php($sl = 1)
              @foreach($coupons as $coupon)
              <tr> 
                  <td>{{ $sl++ }}</td>
                  <td id="t_coupon_name">{{ $coupon->coupon_name }}</td>
                  <td id="t_discount_name">{{ $coupon->discount_name }}</td>
                  <td>
                      @if($coupon->coupon_status == 1)
                      <a id="{{$coupon->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                          <i class="fa fa-arrow-up"></i>
                      </a>
                      @else
                      <a id="{{$coupon->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                          <i class="fa fa-arrow-down"></i>
                      </a>
                      @endif
                  </td>
                  <td>
                      <a id="{{$coupon->id}}" href="#editCouponModal" class="btn btn-success edit" data-toggle="modal">
                          <i class="fa fa-edit"></i>
                      </a>
                      <a id="{{$coupon->id}}" href="" class="btn btn-danger delete">
                          <i class="fa fa-trash"></i>
                      </a>
                  </td>
              </tr>
              
              @endforeach
          </tbody>

          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script>
    
        $(document).ready( function () {
    
            //load table via ajax
            function loadDataTable(){
                $.ajax({
                    url: "{{ route('admin.coupon.getTableData') }}",
                    success: function(data){
                        $('#tableData').html(data);
                    }
                })
            };
            loadDataTable();
            //add new coupon data
            $('#addCouponForm').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.coupon') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if (data == "done") {
                            $('#addCouponModal').modal('hide');
                            $('#addCouponForm')[0].reset();
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Coupon Inserted Successfully !!'
                            })
                        }
                    },
                     error: function (error) {
                        $('#createCouponMessage').html('');

                        $('#createCouponMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#createCouponMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // create modal set to default
            $('#addCouponModal').on('hidden.bs.modal', function (e) {
                let input = $('#addCouponForm #coupon_name');
                $('#addCouponForm').find('#createCouponMessage').html('');
                $(input).val('');
            })

        //coupon publish
        $(document).on('click', '.publish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/coupon/publish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //coupon unpublish
        $(document).on('click', '.unpublish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/coupon/unpublish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                    
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //show data for edit modal
        $(document).on('click', '.edit', function(e){
            $('#editCouponModal').modal('show');
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/coupon/edit')}}/"+id,
                method: "GET",
                success: function(data){
                        $('#edit_id').val(data.id);
                        $('#edit_coupon_name').val(data.coupon_name);
                        $('#edit_discount_name').val(data.discount_name);
                }
            })
        });
        //update coupon
        $('#editCouponForm').on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.coupon.update') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if (data == "done") {
                            $('#editCouponModal').modal('hide');
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Coupon Updated Successfully !!'
                            })
                        }
                    },
                    error: function (error) {
                        $('#updateCouponMessage').html('');

                        $('#updateCouponMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#updateCouponMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // Update modal set to default
            $('#editCouponModal').on('hidden.bs.modal', function (e) {
            let input = $('#editCouponForm #edit_coupon_name');
            $('#editCouponForm').find('#updateCouponMessage').html('');
            $(input).val('');
            })
        //delete coupon
        $(document).on('click', '.delete', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                      if (result.value) {
                        $.ajax({
                            url: "{{url('admin/coupon/delete')}}/"+id,
                            method: "GET",
                            success: function(data){
                                if (data == "done") {
                                    loadDataTable();
                                }
                            }
                        })
                        Swal.fire(
                          'Deleted!',
                          'Coupon has been deleted.',
                          'success'
                        )
                      }
                    })
            
        });
    
    
        //show modal after error occured
        @if (count($errors) > 0)
            $('#addCatModal').modal('show');
        @endif

        } );
        //popover
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
        //tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
     
        {{--Add coupon modal here--}}
        @include('admin.coupon.add-coupon')

        {{--Edit coupon modal here--}}
        @include('admin.coupon.edit-coupon')

@endsection