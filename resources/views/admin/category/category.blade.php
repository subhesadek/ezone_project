@extends('admin.admin_layouts')

@section('admin_content')

      <div class="sl-page-title">
        <h5>Category Table</h5>
        </div><!-- sl-page-title -->

      <div class="card pd-20 pd-sm-40">
        <span class="h4">Category List</span>
        <button class="btn btn-primary float-right" data-toggle="modal" data-target="#addCatModal">
        <i class="fa fa-plus"><b> Add New</b></i>
        </button>
        <br>
        <div class="table-wrapper">
          <table id="datatable1" class="table display responsive nowrap table-sm">
            <thead>
              <tr>
                <th>Sl No.</th>
                <th>Category Name</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="tableData">
              @php($sl = 1)
              @foreach($categories as $category)
              <tr> 
                  <td>{{ $sl++ }}</td>
                  <td id="t_cat_name" class="text-capitalize">{{ $category->cat_name }}</td>
                  <td>
                      @if($category->cat_status == 1)
                      <a id="{{$category->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                          <i class="fa fa-arrow-up"></i>
                      </a>
                      @else
                      <a id="{{$category->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                          <i class="fa fa-arrow-down"></i>
                      </a>
                      @endif
                  </td>
                  <td>
                      <a id="{{$category->id}}" href="#editCatModal" class="btn btn-success edit" data-toggle="modal">
                          <i class="fa fa-edit"></i>
                      </a>
                      <a id="{{$category->id}}" href="" class="btn btn-danger delete">
                          <i class="fa fa-trash"></i>
                      </a>
                  </td>
              </tr>
              
              @endforeach
          </tbody>

          </table>
        </div><!-- table-wrapper -->
      </div><!-- card -->
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
      <script>
    
        $(document).ready( function () { 
            //for datatable
            // $('#datatable1').DataTable();
    
            //load table via ajax
            function loadDataTable(){
                $.ajax({
                    url: "{{ route('admin.category.getTableData') }}",
                    success: function(data){
                        $('#tableData').html(data);
                    }
                })
            };
            loadDataTable();
            //add new category data
            $('#addCategoryForm').on('submit', function(e){
                //var table = $('#category').DataTable();
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.category') }}",
                    data: new FormData(this),
                    /*data: $('addCategoryForm').serialize(),*/
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if (data == "done") {
                            $('#addCatModal').modal('hide');
                            $('#addCategoryForm')[0].reset();
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Category Inserted Successfully !!'
                            })
                        }
                    },
                     error: function (error) {
                        $('#createCategoryMessage').html('');

                        $('#createCategoryMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#createCategoryMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // create modal set to default
            $('#addCatModal').on('hidden.bs.modal', function (e) {
                let input = $('#addCategoryForm #cat_name');
                $('#addCategoryForm').find('#createCategoryMessage').html('');
                $(input).val('');
            })

        //category publish
        $(document).on('click', '.publish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/category/publish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //category unpublish
        $(document).on('click', '.unpublish', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/category/unpublish')}}/"+id,
                method: "GET",
                beforeSend: function(){
                    $('.loader').show();
                    
                },
                complete: function(){
                    $('.loader').hide();
                },
                success: function(data){
                        if (data == "done") {
                            loadDataTable();
                        };
                }
            })
        });
        //show data for edit modal
        $(document).on('click', '.edit', function(e){
            $('#editCatModal').modal('show');
            e.preventDefault();
            var id = $(this).attr('id');
            $.ajax({
                url: "{{url('admin/category/edit')}}/"+id,
                method: "GET",
                success: function(data){
                        $('#edit_id').val(data.id);
                        $('#edit_cat_name').val(data.cat_name);
                }
            })
        });
        //update category
        $('#editCategoryForm').on('submit', function(e){
                //var table = $('#category').DataTable();
                e.preventDefault();
                $.ajax({
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    method: "POST",
                    url: "{{ route('admin.category.update') }}",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function(data){
                        if (data == "done") {
                            $('#editCatModal').modal('hide');
                            loadDataTable();
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Category Updated Successfully !!'
                            })
                        }
                    },
                    error: function (error) {
                        $('#updateCategoryMessage').html('');

                        $('#updateCategoryMessage').append('<ul id="errorMessage" class="alert alert-danger"></ul>')

                        $.each(error.responseJSON.errors, function (index, value) {
                            console.log(value[0]);
                            $('#updateCategoryMessage').find('#errorMessage').append(`
                                    <li>` + value[0] + ` </li>
                                `);
                        });
                    }
                });
            });

            // Update modal set to default
            $('#editCatModal').on('hidden.bs.modal', function (e) {
            let input = $('#editCategoryForm #edit_cat_name');
            $('#editCategoryForm').find('#updateCategoryMessage').html('');
            $(input).val('');
            })
        //delete category
        $(document).on('click', '.delete', function(e){
            e.preventDefault();
            var id = $(this).attr('id');
            Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    }).then((result) => {
                      if (result.value) {
                        $.ajax({
                            url: "{{url('admin/category/delete')}}/"+id,
                            method: "GET",
                            success: function(data){
                                if (data == "done") {
                                    loadDataTable();
                                }
                            }
                        })
                        Swal.fire(
                          'Deleted!',
                          'Category has been deleted.',
                          'success'
                        )
                      }
                    })
            
        });
    
    
        //show modal after error occured
        @if (count($errors) > 0)
            $('#addCatModal').modal('show');
        @endif

        } );
        //popover
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
        //tooltip
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
     
        {{--Add category modal here--}}
        @include('admin.category.add-category')

        {{--Edit category modal here--}}
        @include('admin.category.edit-category')

@endsection