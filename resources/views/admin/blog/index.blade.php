@extends('admin.admin_layouts')
@section('admin_content')

  <!-- ########## START: MAIN PANEL ########## -->
 
        <div class="sl-page-title">
          <h5>Post Table</h5>
          </div><!-- sl-page-title -->

        <div class="card pd-20 pd-sm-40">
          <span class="h4">Product List</span>
          <a class="btn btn-primary float-right" href="{{ route('add.blogpost') }}">
          <i class="fa fa-plus"><b> Add New Post</b></i>
          </a><br><br>
          <div class="table-wrapper">
            <table id="datatable1" class="table display responsive nowrap">
              <thead>
                <tr>
                  <th class="wd-15p">Post Title</th>
                  <th class="wd-15p">Category</th>
                  <th class="wd-15p">Image</th>
                  <th class="wd-20p">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach($post as $row)
                <tr>
                  <td>{{$row->post_title_en}}</td>
                  <td>{{$row->category_name_en}}</td>
                  <td><img src="{{ URL::to($row->post_image)}}" width="50px;" height="50px;"></td>
                  <td>
                  <a href="{{ URL::to('admin/edit/post/' .$row->id)}}" class="btn btn-sm btn-info" title="Edit"><i class="fa fa-edit"></i></a>
                  <a href="{{ URL::to('admin/delete/post/' .$row->id)}}" class="btn btn-sm btn-danger" id="delete" title="Delete"><i class="fa fa-trash"></i></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div><!-- table-wrapper -->

          <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
          <script>  
            $(document).on("click", "#delete", function(e){
                e.preventDefault();
                var link = $(this).attr("href");
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                    })
                   .then((result) => {
                     if (result.value) {
                          window.location.href = link;
                     } else {
                       swal("Safe Data!");
                     }
                   });
               });
          </script>
@endsection