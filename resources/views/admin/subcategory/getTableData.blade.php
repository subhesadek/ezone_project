                                @php($sl = 1)
                                        @foreach($subcategory as $subcategory)
                                        <tr>
                                            <td>{{ $sl++ }}</td>
                                            <td id="t_subcat_name" data-id1="{{ $subcategory->id }}"  class="text-capitalize">{{ $subcategory->subcategory_name }}</td> 
                                            <td id="t_cat_name" data-id1="{{ $subcategory->category_id }}"  class="text-capitalize">{{ $subcategory->cat_name }}</td> 
                                            <td>
                                                @if($subcategory->subcat_status == 1)
                                                <a id="{{$subcategory->id}}" href="" class="btn btn-primary unpublish" data-toggle="tooltip" title="Published">
                                                    <i class="fa fa-arrow-up"></i>
                                                </a>
                                                @else
                                                <a id="{{$subcategory->id}}" href="" class="btn btn-warning publish" data-toggle="tooltip" title="Unpublished">
                                                    <i class="fa fa-arrow-down"></i>
                                                </a> 
                                                @endif
                                            </td>
                                            <td>
                                                <a id="{{$subcategory->id}}" href="#editSubcatModal" class="btn btn-success edit" data-toggle="modal">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a id="{{$subcategory->id}}" href="" class="btn btn-danger delete">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        {{--Edit category modal here--}}
                                         @include('admin.subcategory.edit-subcategory')
                                        @endforeach