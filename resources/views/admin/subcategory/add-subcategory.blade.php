<div class="modal fade" id="addSubcatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mg-r-100 mg-l-100 " id="exampleModalLabel">Add Subcategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> 
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form id="addSubcategoryForm" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <span id="createSubcategoryMessage"></span>
                    <div class="form-group">
                        <label for="cat_name">Subcategory Name</label>
                        <input type="text" name="subcategory_name" class="form-control text-capitalize" id="subcategory_name" placeholder="Enter Subcategory Name">
                    </div>
                    <div class="form-group">
                        <label for="cat_name">Category Name</label>
                        {{-- <input type="text" name="cat_name" class="form-control text-capitalize" id="cat_name" placeholder="Enter Category Name"> --}}
                        <select class="form-control text-capitalize" name="category_id" id="category_id">
                        @foreach ($category as $row)
                          <option value="{{ $row->id }}">{{ $row->cat_name }}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group"> 
                        <label for="subcat_status">Publication Status</label>
                        <select class="form-control" name="subcat_status" id="subcat_status">
                            <option value="1">Published</option>
                            <option value="0">Unpublished</option>
                        </select>
                    </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="add_button" class="btn btn-primary">Add Subcategory</button>
                </div>
            </form>
        </div>
    </div>
</div>
