<div class="modal fade" id="editSubcatModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title mg-r-100 mg-l-100" id="exampleModalLabel">Edit Subcategory</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @if ($errors->any())
            <div class="alert alert-danger"> 
                <ul> 
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            <form id="editSubcategoryForm" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="id" id="edit_id">
                <div class="modal-body">
                    <span id="updateSubcategoryMessage"></span>
                    <div class="form-group">
                        <label for="subcat_name">Subcategory Name</label>
                        <input type="text" name="subcategory_name" class="form-control text-capitalize" id="edit_subcat_name">
                    </div>
                    <div class="form-group">
                        <label for="cat_name">Category Name</label>
                        {{-- <input type="text" name="cat_name" class="form-control text-capitalize" id="edit_cat_name" placeholder="Enter Category Name"> --}}
                        <select class="form-control text-capitalize" name="category_id" id="edit_cat_name">
                            @foreach ($category as $row)
                              <option value="{{ $row->id }}" 
                                @if($row->id == $subcategory->category_id) selected='selected' @endif 
                                >{{ $row->cat_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Category</button>
                </div>
            </form>
        </div> 
    </div>
</div>
