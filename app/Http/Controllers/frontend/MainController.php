<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Admin\Category;
use App\Model\Admin\Subcategory;
use App\Model\Admin\Brand;
use DB;

class MainController extends Controller
{
    public function getCategories(){
        $categories = Category::where('cat_status', 1)->get();
        return response()->json([
            'categories'=>$categories
        ], 200);
        // return response()->json($categories);
    }

    public function getSubcategories(){
    	$subcategories = Subcategory::where('subcat_status', 1)->limit(9)->get();
        return response()->json($subcategories);
    }

    public function getBrands(){
    	$brands = Brand::where('brand_status', 1)->get();
        return response()->json($brands);
    }

    public function getProducts(){
    	$products = DB::table('products')->where('status', 1)->get();
    	return response()->json([
            'products'=>$products
        ], 200);
    }

    public function getcatProducts($id){
        $catProduct = DB::table('products')->where('category_id', $id)
                    ->where('status', 1)
                    ->orderBy('id', 'desc')
                    ->get();
        return response()->json([
            'products'=>$catProduct
        ], 200);
        // return response()->json($catProduct);
    }

}
