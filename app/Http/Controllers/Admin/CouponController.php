<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Admin\Coupon;
use DB;

class CouponController extends Controller
{ 
    public function index(){
        $coupon = Coupon::orderby('id', 'DESC')->get();
        return view('admin.coupon.coupon', ['coupons' => $coupon]);
    }
    public function get(){
        $d['coupons'] = Coupon::orderby('id', 'DESC')->get();
        return view('admin.coupon.getTableData', $d);
    }

    public function create(Request $request){
        $request->validate([
            'coupon_name' => 'required|unique:coupons|max:255',
            'discount_name' => 'required|max:4',
        ]);

        $coupon = new Coupon();
        $coupon->coupon_name = $request->coupon_name;
        $coupon->discount_name = $request->discount_name;
        $coupon->coupon_status = $request->coupon_status;
        $coupon->save();

        echo "done";
    }

    public function edit($id){
        $coupon = Coupon::find($id);
        return response()->json($coupon);
    }
 
    public function update(Request $request){
        $request->validate([
            'coupon_name' => 'required|max:255',
            'discount_name' => 'required|max:4',
        ]); 
        $coupon = Coupon::find($request->id);
        $coupon->coupon_name = $request->coupon_name;
        $coupon->discount_name = $request->discount_name;
        $coupon->save();

        echo "done";
    }
 


    public function delete($id){
        $coupon = Coupon::find($id);
        
        if ($coupon){
            $coupon->delete();
        }
        echo "done";
    }

    public function publish($id){
        $coupon = Coupon::find($id);
        $coupon->coupon_status = 1;
        $coupon->save();

        echo "done";
    }

    public function unpublish($id){
        $coupon = Coupon::find($id);
        $coupon->coupon_status = 0;
        $coupon->save();

        echo "done";
    }
}
