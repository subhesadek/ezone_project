<?php

namespace App\Http\Controllers\Admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
// use Illuminate\Http\return response()->json($data, 200, $headers);;
use App\Model\Admin\Subcategory;
use App\Model\Admin\Category;
use DB;
class SubcategoryController extends Controller
{

    public function index(){
        $category = DB::table('categories')->get();
        $subcategory = DB::table('subcategories')
        ->join('categories', 'subcategories.category_id', 'categories.id')
        ->select('subcategories.*', 'categories.*')
        ->get();
        // $subcategory = Subcategory::orderby('id', 'DESC')->get();
        return view('admin.subcategory.subcategory', compact('category','subcategory'));
    }
    public function get(){        
        $category = DB::table('categories')->get();
        $subcategory = DB::table('subcategories')
        ->join('categories', 'subcategories.category_id', 'categories.id')
        ->select('subcategories.*', 'categories.cat_name')->orderby('id', 'DESC')
        ->get();

        // $d['subcategories'] = Subcategory::orderby('id', 'DESC')->get();
        return view('admin.subcategory.getTableData', compact('category','subcategory'));
    }

    public function create(Request $request){
        $request->validate([
            'subcategory_name' => 'required|unique:subcategories|max:255',
            'category_id' => 'required',
        ]);

        $subcategory = new Subcategory();
        $subcategory->category_id = $request->category_id;
        $subcategory->subcategory_name = $request->subcategory_name;
        $subcategory->subcat_status = $request->subcat_status;
        $subcategory->save();

        echo "done";
    }

    public function edit($id){
        $subcategory = Subcategory::find($id);
        // $category =Category::get();
        //  return response()->json(array('Subcategory'=>$subcategory,'Category'=>$category));
        return response()->json($subcategory);
        // return response()->json($subcategory, 200, $category);
    }
 
    public function update(Request $request){
        $request->validate([
            'subcategory_name' => 'required|max:255',
        ]); 
        $subcategory = Subcategory::find($request->id);
        $subcategory->category_id = $request->category_id;
        $subcategory->subcategory_name = $request->subcategory_name;
        $subcategory->save();

        echo "done";
    }
 


    public function delete($id){
        $subcategory = Subcategory::find($id);
        
        if ($subcategory){
            $subcategory->delete();
        }
        echo "done";
    }

    public function publish($id){
        $subcategory = Subcategory::find($id);
        $subcategory->subcat_status = 1;
        $subcategory->save();

        echo "done";
    }

    public function unpublish($id){
        $subcategory = Subcategory::find($id);
        $subcategory->subcat_status = 0;
        $subcategory->save();

        echo "done";
    }
} 
