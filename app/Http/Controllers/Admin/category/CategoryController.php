<?php

namespace App\Http\Controllers\Admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Admin\Category;
use DB;
class CategoryController extends Controller
{

    public function index(){
        $category = Category::orderby('id', 'DESC')->get();
        return view('admin.category.category', ['categories' => $category]);
    }
    public function get(){
        $d['categories'] = Category::orderby('id', 'DESC')->get();
        return view('admin.category.getTableData', $d);
    }

    public function create(Request $request){
        $request->validate([
            'cat_name' => 'required|unique:categories|max:255',
        ]);

        $category = new Category();
        $category->cat_name = $request->cat_name;
        $category->cat_status = $request->cat_status;
        $category->save();

        echo "done";
    }

    public function edit($id){
        $category = Category::find($id);
        return response()->json($category);
    }
 
    public function update(Request $request){
        $request->validate([
            'cat_name' => 'required|max:255',
        ]); 
        $category = Category::find($request->id);
        $category->cat_name = $request->cat_name;
        $category->save();

        echo "done";
    }
 


    public function delete($id){
        $category = Category::find($id);
        
        if ($category){
            $category->delete();
        }
        echo "done";
    }

    public function publish($id){
        $category = Category::find($id);
        $category->cat_status = 1;
        $category->save();

        echo "done";
    }

    public function unpublish($id){
        $category = Category::find($id);
        $category->cat_status = 0;
        $category->save();

        echo "done";
    }
} 
