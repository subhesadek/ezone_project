<?php

namespace App\Http\Controllers\Admin\category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Admin\Brand;
use DB; 
class BrandController extends Controller
{

    public function index(){
        $brand = Brand::orderby('id', 'DESC')->get();
        return view('admin.brand.brand', ['brands' => $brand]);
        // return view('admin.brand.brand');
    }
    public function get(){
        $d['brands'] = Brand::orderby('id', 'DESC')->get();
        return view('admin.brand.getTableData', $d);
    }

    public function create(Request $request){
        $request->validate([
            'brand_name' => 'required|unique:brands|max:255',
            'brand_logo' => 'image',
        ]);
        $imageUrl = '';
        if ($file = $request->file('brand_logo')) {
            $fileName = $file->getClientOriginalName();
            $directory = 'backend/images/brand/';
            $imageUrl = $directory.$fileName;
            $file->move($directory, $fileName);
        }

        $brand = new Brand();
        $brand->brand_name = $request->brand_name;
        $brand->brand_logo = $imageUrl;
        $brand->brand_status = $request->brand_status;
        $brand->save();

        echo "done";
    }

    public function edit($id){
        $brand = Brand::find($id);
        return response()->json($brand);
    }
 
    public function update(Request $request){
        $request->validate([
            'brand_name' => 'required|max:255',
            'brand_logo' => 'image',
        ]);
        $brand = Brand::find($request->id);
        if ($file = $request->file('brand_logo')) {
            unlink($brand->brand_logo);
            $fileName = $file->getClientOriginalName();
            $directory = 'backend/images/brand/';
            $imageUrl = $directory.$fileName;
            $file->move($directory, $fileName);
            $brand->brand_logo = $imageUrl;
            $brand->save();
            
        }

        $brand->brand_name = $request->brand_name;
        $brand->save();

        echo "done";
    }
 
    public function inlineEdit(Request $request){
        $brand = Brand::find($request->id);
        if ($request->column_name == "brand_name") {
            $brand->brand_name = $request->text;
        }
        $brand->save();
        
        echo "done";
    }

    public function delete($id){
        $brand = Brand::find($id);
        $image= $brand->brand_logo;
        
        if ($brand){
            $brand->delete();
        }elseif($image){
            unlink($image);
        }
        echo "done";
    }

    public function publish($id){
        $brand = Brand::find($id);
        $brand->brand_status = 1;
        $brand->save();

        echo "done";
    }

    public function unpublish($id){
        $brand = Brand::find($id);
        $brand->brand_status = 0;
        $brand->save();

        echo "done";
    }
} 
